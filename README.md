# Aetersum

## Description

Aetersum is a project designed to implement Lunamalis using an authoritative
server and client relationship.

## Table of Contents

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Aetersum](#aetersum)
	- [Description](#description)
	- [Table of Contents](#table-of-contents)
	- [Prerequisites](#prerequisites)
	- [Installation](#installation)
	- [Usage](#usage)
	- [Contributing](#contributing)
		- [All contributions you make will be under the Apache License 2.0.](#all-contributions-you-make-will-be-under-the-apache-license-20)
		- [Issues and Bugs](#issues-and-bugs)
		- [Coding Style](#coding-style)
		- [Committing](#committing)
	- [Credits](#credits)
	- [License](#license)

<!-- /TOC -->

## Prerequisites

*   pipenv
*   yattag
*   Tornado
*   Python 3.5.2
*   MySQL

## Installation

1.  Download this repository.
1.  The Pipfile has all the [prerequisites](#prerequisites). Install them using `pipenv install .`.
1.  Modify the `aetersum/config.py` file as needed.

## Usage

1.  Run `./start.sh` to start the server.
1.  Using a web browser of your choice, connect to `http://localhost:8999`.

## Contributing

### All contributions you make will be under the Apache License 2.0.

Whenever you submit any changes to this project, what you have submitted will
be under the same Apache License 2.0 that covers this project. If you are
concerned about this, please contact the maintainers.

### Issues and Bugs

Use Gitlab's issue tracker to submit bugs. Issues are not to be used for asking
questions. Instead, contact the maintainers or join a discussion board.

Please write your bug reports with as much detail and background as possible.
There is no such thing as "too much detail," as bugs can have many causes.
Good bug reports tend to have:

*  A summary
*  Steps to reproduce
*  What you expected to happen
*  What actually happens
*  The version of Aetersum you are running
*  The specifications of the machine you are running Aetersum on
*  Any miscellaneous notes

### Coding Style

You can technically use whatever coding style you want, as long as you are
consistant. Typically, we like to use

*  [Google's PyGuide](https://github.com/google/styleguide/blob/gh-pages/pyguide.md) for Python
*  [Google's Javascript Style Guide](https://google.github.io/styleguide/jsguide.html)
*  [Google's HTML and CSS Style Guide](https://google.github.io/styleguide/htmlcssguide.html)

We prefer that you use four spaces per indentation level. If you want to use
tabs, note that you are not alone, but try to set up your editor to
automatically convert between the two.

### Committing

You may use either present tense or past tense. Try to limit the first line of
a commit message to 72 characters or less; the first line is usually used as
the title for a commit.

## Credits

See the credits file at [CREDITS.md](CREDITS.md)

## License

We use the Apache License 2.0. The license may be viewed at [LICENSE](LICENSE)
