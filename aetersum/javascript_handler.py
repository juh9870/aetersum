#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import tornado.template

class JSCompiler:
	def __init__(self, javascript_files, template_dict):
		self.javascript_files = javascript_files
		self.template_dict = template_dict

	def do(self):
		write_end = []
		upper_path = os.path.join(
			os.getcwd(),
			'aetersum',
			'javascript',
			'upper.js'
		)
		lower_path = os.path.join(
			os.getcwd(),
			'aetersum',
			'javascript',
			'lower.js'
		)
		for js_file in self.javascript_files:
			if 'no_upper' not in js_file:
				with open(
					upper_path,
					'r'
				) as upper_f:
					# print(str(**self.template_dict))
					upper_f_template = tornado.template.Template(upper_f.read())
					write_end.append(
						upper_f_template.generate(template_dict=self.template_dict)
					)

			for js_file_individual in js_file['from']:
				file_get_from = os.path.join(
					os.getcwd(),
					'aetersum',
					'javascript' + js_file_individual
				)
				with open(file_get_from, 'r') as file_got:
					tmp_template = tornado.template.Template(file_got.read())
					write_end.append(
						tmp_template.generate(template_dict=self.template_dict)
					)

			if 'no_lower' not in js_file:
				with open(
					lower_path,
					'r'
				) as lower_f:
					lower_f_template = tornado.template.Template(lower_f.read())
					write_end.append(
						lower_f_template.generate(template_dict=self.template_dict)
					)
		return b''.join(write_end)

def main():
	pass

if __name__ == '__main__':
	main()
