/**
 * [stateStarMap description]
 * @type {Object}
 */
let stateStarMap = {
	mapData: {},
	/**
	 * Preloads the star map state.
	 * @return {undefined} No need to return anything.
	 */
	preload: function(){
		lunamalis.ws.ws.onmessage = function(event){
			let response = lunamalis.ws.onMessage(event);
			let responseJ = undefined;
			let responseA = undefined;
			if(response[0] === 'j'){
				responseJ = JSON.parse(response[1]);
				if(responseJ.hasOwnProperty('nearbySystems')){
					stateStarMap.drawSystems(responseJ.nearbySystems);
				}
				if(responseJ.hasOwnProperty('currentSystemId')){
					stateStarMap.setCameraToSystem(responseJ.currentSystemId);
				}
			}
			else if(response[0] === 'a'){
				responseA = response[1].split(' ');
				if(responseA[0] === 'NoMove'){
					stateStarMap.statusSet('You can not move to that system.')
				}
			}
			console.log('got:',responseJ || responseA);

		}
		document.getElementById(game.parent).addEventListener(
			'wheel',
			stateStarMap.mouseWheelCallback
		);
	},
	/**
	 * Creates the star map state.
	 * @return {undefined} No need to return anything.
	 */
	create: function(){
		console.log('Create');
		game.physics.startSystem(Phaser.Physics.ARCADE);
		game.stage.backgroundColor = '#000';
		game.world.setBounds(
			-{{template_dict['config'].universe['width']}},
			-{{template_dict['config'].universe['height']}},
			{{template_dict['config'].universe['width']}},
			{{template_dict['config'].universe['height']}}
		);
		// Used for zooming.
		stateStarMap.gameWorld = game.add.group();
		stateStarMap.gameWorld.position.setTo(game.world.centerX, game.world.centerY);
		stateStarMap.gameWorldScale = 1;
		// </>
		game.camera.setSize(500, 500);
		game.camera.width = 500;
		game.camera.height = 500;
		game.camera.bounds = (0, 0, 500, 500);
		game.camera.setSize(500, 500);

		/**
		 * Contains labels.
		 * @type {Object}
		*/
		this.labels = {};
		/**
		 * When clicked, takes you to the ship view.
		 * @type {label}
		*/
		this.labels['viewShip'] = game.add.text(0,0,'Ship\nView',{font:'10px Arial',fill:'#fff'});
		this.labels['viewShip'].fixedToCamera = true;
		this.labels['viewShip'].cameraOffset.setTo(0, game.height-this.labels['viewShip'].height);

		this.labels['point'] = game.add.text(0,0, '0, 0', {font:'10px Arial', fill:'#fff'});
		this.labels['point'].fixedToCamera = true;
		this.labels['point'].cameraOffset.setTo(0, 0);

		this.mothership = new Mothership();
		this.mothership.create();
		stateStarMap.gameWorld.add(this.mothership.ship);
		game.camera.follow(this.mothership.ship, Phaser.Camera.FOLLOW_LOCKON, .5, .5);

		let saveN = lunamalis.ls.get().saveN;
		if(saveN !== undefined){
			lunamalis.ws.sendA(['setSave',saveN]);
		}
		else{
			alert('You don\'t have any saves yet. Redirecting you to the save page.');
			window.location = 'saves';
		}
		lunamalis.ws.sendA(['getMap']);



	},
	update: function(){
		this.labels['point'].text = this.mothership.ship.x + ', ' +
		this.mothership.ship.y + '\n' + stateStarMap.gameWorldScale;

		// game.camera.setPosition(this.mothership.ship.x, this.mothership.ship.y);
		// game.camera.setPosition(0, 0);
		// game.camera.x = this.mothership.ship.x;
		// game.camera.y = this.mothership.ship.y;
		// game.camera.focusOnXY(0, 0)

		if(game.input.keyboard.isDown(Phaser.Keyboard.UP)){
			stateStarMap.mothership.ship.y--;
			stateStarMap.gameWorld.pivot.y--;
		}
		else if(game.input.keyboard.isDown(Phaser.Keyboard.DOWN)){
			stateStarMap.mothership.ship.y++;
			stateStarMap.gameWorld.pivot.y++;
		}
		if(game.input.keyboard.isDown(Phaser.Keyboard.LEFT)){
			stateStarMap.mothership.ship.x--;
			stateStarMap.gameWorld.pivot.x--;
		}
		else if(game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)){
			stateStarMap.mothership.ship.x++;
			stateStarMap.gameWorld.pivot.x++;
		}

		// Update zoom.
		stateStarMap.gameWorld.scale.set(stateStarMap.gameWorldScale);
	},
	systems: {},
	hyperlanes: {},
	drawSystems: function(systems){
		for(let systemI = 0; systemI<systems.length; systemI++){
			console.log(
				'Drawing system',
				systems[systemI].id,
				'at',
				systems[systemI].point[0],
				',',
				systems[systemI].point[1]
			);
			let system = {
				data: systems[systemI],
				g: {
					main: game.add.graphics(0,0).beginFill(0xffffff).drawCircle(
						0, 0, 10
					).endFill()
				},
				t: game.add.text(0, 0, systems[systemI].id, {font: '10px Arial', fill:'#fff'})
			};
			system.g.main.x = systems[systemI].point[0];
			system.g.main.y = systems[systemI].point[1];
			system.t.x = systems[systemI].point[0];
			system.t.y = systems[systemI].point[1] + 10;
			system.s = {
				main: game.add.sprite(
					systems[systemI].point[0],
					systems[systemI].point[1],
					system.g.main.generateTexture()
				)
			};
			system.s.main.anchor.x = 0.5;
			system.s.main.anchor.y = 0.5;
			system.g.main.destroy();
			system.g.main = null;
			delete system.g.main;
			system.s.main.data.lunamalis = {
				systemId: systems[systemI].id
			};
			system.s.main.x = systems[systemI].point[0];
			system.s.main.y = systems[systemI].point[1];
			system.s.main.inputEnabled = true;
			system.s.main.events.onInputUp.add(stateStarMap.moveToSystem);

			// Some systems are the home systems of factions. They need a star
			// symbol around them to symbolize this.
			if(system.data.home !== undefined){
				system.g.home = game.add.graphics(0,0).beginFill(0xffffff)
					.drawPolygon(
						// [0, 100, 100, 0, 100, 100]
						[
							// https://www.desmos.com/calculator/mt5uymg2yp (own work)
							10, 18, // A
							12, 12, // B
							18, 12, // C
							14, 8, // D
							16, 2, // E
							10, 6, // F
							4, 2, // G
							6, 8, // H
							8, 12, // I
							2, 12 // J
						]
					).endFill();
				// system.g.home.anchor.x = 0.5;
				// system.g.home.anchor.y = 0.5;
				system.s.main.addChild(system.g.home);
			}

			stateStarMap.gameWorld.add(system.s.main);



			for(
				let hyperlaneI = 0;
				hyperlaneI < system.data.hyperlanes.length;
				hyperlaneI++
			){
				// Check to see if the hyperlane points to a system that is
				// already on the map.
				if(stateStarMap.systems[system.data.hyperlanes[hyperlaneI]] !== undefined){
					let hyperlane = {
						to: system.data.hyperlanes[hyperlaneI],
						from: system.data.id,
						// g: {
						// 	main: game.add.graphics(0,0).beginFill(0xffffff).lineStyle(
						// 		1,
						// 		0xffffff,
						// 		1
						// 	).moveTo(
						// 		stateStarMap.systems[system.data.hyperlanes[hyperlaneI]].data.point[0],
						// 		stateStarMap.systems[system.data.hyperlanes[hyperlaneI]].data.point[1]
						// 	).lineTo(
						// 		system.data.point[0],
						// 		system.data.point[1]
						// 	).endFill()
						// },
						// s: null
					};
					// hyperlane.s = {
					// 	main: game.add.sprite(
					// 		system.data.point[0],
					// 		system.data.point[1],
					// 		hyperlane.g.main.generateTexture()
					// 	)
					// }
					// hyperlane.g.main.destroy();
					// hyperlane.g.main = null;
					// delete hyperlane.g
					// hyperlane.l = new Phaser.Line(
					// 	stateStarMap.systems[system.data.hyperlanes[hyperlaneI]].data.point[0],
					// 	stateStarMap.systems[system.data.hyperlanes[hyperlaneI]].data.point[1],
					// 	system.data.point[0],
					// 	system.data.point[1]
					// )
					hyperlane.g = {};
					hyperlane.g.line = game.add.graphics(0,0, stateStarMap.gameWorld);
					hyperlane.g.line.lineStyle(1, 0x2073f9, 1);
					hyperlane.g.line.moveTo(
						stateStarMap.systems[system.data.hyperlanes[hyperlaneI]].data.point[0],
						stateStarMap.systems[system.data.hyperlanes[hyperlaneI]].data.point[1]
					)
					hyperlane.g.line.lineTo(
						system.data.point[0],
						system.data.point[1]
					)
					stateStarMap.hyperlanes[hyperlane.from] = stateStarMap.hyperlanes[hyperlane.from] || [];
					stateStarMap.hyperlanes[hyperlane.from].push(hyperlane);

				}
			}
			console.log('system:', system);
			stateStarMap.systems[system.data.id] = system;
		}
	},
	systemDataNode: document.getElementById('systemData'),
	setCameraToSystem: function(systemId){
		// game.camera.setPosition(stateStarMap.systems[systemId].s.main.x, stateStarMap.systems[systemId].s.main.y);
		this.mothership.ship.x = stateStarMap.systems[systemId].s.main.x;
		this.mothership.ship.y = stateStarMap.systems[systemId].s.main.y;
		// this.mothership.ship.x = 0;
		// this.mothership.ship.y = 0;
		let systemDataHTML =
			'<h2>'
				+
				stateStarMap.systems[systemId].data.name
				+
			'</h2>'
		;
		if((stateStarMap.systems[systemId].data.fleets !== undefined) && (stateStarMap.systems[systemId].data.fleets.length > 0)){
			systemDataHTML += '<button>Attack Fleets</button>';
		}
		stateStarMap.systemDataNode.innerHTML = systemDataHTML;
	},
	moveToSystem: function(sprite, pointer){
		// console.log('Moving to', arguments);
		console.log('Moving to', sprite.data.lunamalis.systemId);
		lunamalis.ws.sendA(['gotoSystem', sprite.data.lunamalis.systemId.toString()]);
	},
	mouseWheelCallback: function(event){
		if(event.deltaY < 0){
			// Zoom in.
			stateStarMap.gameWorldScale += 0.1;
		}
		else if(event.deltaY > 0){
			// Zoom out.
			stateStarMap.gameWorldScale -= 0.1;
		}
		// Clamp it to between 0.1 and 3, and then get rid of bad 0.300...0004
		// and etc.
		stateStarMap.gameWorldScale = Math.floor(
			Phaser.Math.clamp(stateStarMap.gameWorldScale, 0.1, 3)
			*
			100 // Multiply by 100 so that Math.floor doesn't round to 0.
		) / 100; // Divide by 100 so back to normal.
	},
	statusNode: document.getElementById('status'),
	statusTimeout: null,
	statusSet: function(text){
		clearTimeout(stateStarMap.statusTimeout);
		stateStarMap.statusNode.innerHTML = text;
		stateStarMap.statusTimeout = setTimeout(stateStarMap.statusClear, 2500);
	},
	statusClear: function(){
		stateStarMap.statusNode.innerHTML = '';
	}
};
