function Mothership(){}
Mothership.prototype = {
	create: function(){
		this.ship = game.add.sprite(
			game.world.centerX,
			game.world.centerY,
			game.add.graphics(0,0).beginFill(0xffffff).drawCircle(0,0,5).endFill().generateTexture()
		);
	}
};
