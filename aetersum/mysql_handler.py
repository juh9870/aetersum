#!/usr/bin/env python2.7
# -*- encoding: utf-8 -*-

import os
import pymysql

# try:
# 	with connection.cursor() as cursor:
# 	# Create a new record
# 	sql = "INSERT INTO `users` (`email`, `password`) VALUES (%s, %s)"
# 	cursor.execute(sql, ('webmaster@python.org', 'very-secret'))
#
# 	# connection is not autocommit by default. So you must commit to save
# 	# your changes.
# 	connection.commit()
#
# 	with connection.cursor() as cursor:
# 	# Read a single record
# 	sql = "SELECT `id`, `password` FROM `users` WHERE `email`=%s"
# 	cursor.execute(sql, ('webmaster@python.org',))
# 	result = cursor.fetchone()
# 	print(result)
# finally:
# 	connection.close()

def connect():
	with open('aetersum'+os.sep+'secret_data'+os.sep+'database_config') as db_f:
		dbstring = db_f.read()
		dbarr = dbstring.split('\n')
		dbdata = {
			'host': dbarr[0],
			'user': dbarr[1],
			'password': dbarr[2],
			'db': dbarr[3]
		}

	connection = pymysql.connect(
		host=dbdata['host'],
		user=dbdata['user'],
		password=dbdata['password'],
		db=dbdata['db'],
		charset='utf8mb4',
		cursorclass=pymysql.cursors.DictCursor
	)

	return connection

def test_data(connection):
	try:
		print('Trying')
		with connection.cursor() as cursor:
			sql = 'SELECT * FROM user_primary'
			cursor.execute(sql)
		print('Committing')
		connection.commit()
		result = cursor.fetchone()
		print('Printing result:')
		print(result)
		print('End result')
	finally:
		pass
	print('Done testing')

def create_default(connection):
	# TODO: Create the default tables.
	pass


def main():
	print('Testing...')
	connection = connect()
	test_data(connection)
	connection.close()


if __name__ == '__main__':
	main()
