#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import tornado.web
import yattag

import html_handler

# TODO: Finish this, you may also look at this:
# https://stackoverflow.com/questions/31884257/error-handling-in-tornado
class NotFoundHandler(tornado.web.RequestHandler):
	def prepare(self):  # for all methods
		raise tornado.web.HTTPError(
			status_code=404,
			reason="Invalid resource path."
		)

def main():
	# TODO: Print some debug information if called directly?
	pass

if __name__ == '__main__':
	main()
