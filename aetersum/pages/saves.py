#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import os
import tornado.web
import yattag

import html_handler

class SavesHandler(tornado.web.RequestHandler):
	def initialize(self, **kwargs):
		self.page = kwargs['page']
	# @tornado.web.asynchronous stops it from automatically closing the
	# connection at the end of the function and lets us do it ourselves using
	# self.finish()
	@tornado.web.asynchronous
	def get(self):
		write_end = html_handler.Head(
			page=self.page,
			extra_scripts=['js'+os.sep+'saves']
		).main()
		doc, tag, text, line = yattag.Doc().ttl()
		with tag('body'):
			body = html_handler.Body()
			doc.asis(body.header())
			with tag('p'):
				text('Once you have chosen a save, you can ')
				with tag('a', href="map"):
					text('play')
				text('.')
			with tag('div'):
				with tag('button', id="newSave"):
					text('Create a new save')
			with tag('table', id="saveList"):
				text('')
			doc.asis(body.footer())
		write_end += doc.getvalue()
		self.write(html_handler.HTMLWrapper.main(write_end))
		self.finish()

def main():
	# TODO: Print some debug information if called directly?
	pass

if __name__ == '__main__':
	main()
