#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

import os
import tornado.template
import tornado.web

class CSSHandler(tornado.web.RequestHandler):
	css_files = [
		{
			'from': [os.sep+'css.css'+os.sep+'css.css']
		}
	]
	def initialize(self, **template_dict):
		self.template_dict = template_dict
	# @tornado.web.asynchronous stops it from automatically closing the
	# connection at the end of the function and lets us do it ourselves using
	# self.finish()
	@tornado.web.asynchronous
	def get(self):
		write_end = []
		for css_file in self.css_files:

			for css_file_individual in css_file['from']:
				file_get_from = os.path.join(
					os.getcwd(),
					'aetersum'+os.sep+'css' + css_file_individual
				)
				with open(file_get_from, 'r') as file_got:
					tmp_template = tornado.template.Template(file_got.read())
					write_end.append(
						tmp_template.generate(template_dict=self.template_dict)
					)

		self.set_header("Content-Type", 'text/css; charset="utf-8"')
		# It gets byte objects, not string.
		self.write(b''.join(write_end))
		self.finish()

def main():
	# TODO: Print some debug information if called directly?
	pass

if __name__ == '__main__':
	main()
