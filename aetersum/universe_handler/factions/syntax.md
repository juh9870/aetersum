# Syntax

This document details the syntax for faction files. For this, the Ganeebian
Research Mandate will be used as an example.

```
{
	# The general name of the faction.
	"ganeebian": {
		# The class must be faction. Not yet used for anything.
		"class": "faction",
		# The colors of the faction. Use a hexadecimal format, and do not
		# prepend a 0x or a #.
		"color": {
			# The primary color.
			primary: "00a867"
		},
		# Generation settings.
		"generation": {
			# How many systems should it spawn with?
			"min_systems": 3,
			"max_systems": 5,
			# What species should it spawn with?
			"include_species": {
				# The key is equivalent to the title of the species in their
				# file. The value is a float, where 1.0 equals 100% of the
				# population.
				"ganeebian": 1.0
			}
		},
		# The name of the faction.
		"name": "Ganeebian Research Mandate",
		# A brief description of the faction.
		"desc": "",
		# Personality details.
		"personality": {
			# Use a specific personality. In the future, you will be able to
			# change the details after this.
			"extends": "fanatic_researchers"
		}
	}
}
```
