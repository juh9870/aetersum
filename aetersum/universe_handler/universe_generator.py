#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import config
import fleet_handler

import os
import random
import json
import math
import time

class UniverseGenerator:
	def __init__(self, **kwargs):

		self.user_id = kwargs.get('user_id', 0)
		self.user_save_id = kwargs.get('user_save_id', 0)
		self.mod_m = kwargs['mod_m']

		# Some of these were generated from tables found online.
		#
		# Used for random.choice. See http://web.archive.org/save/https://stackoverflow.com/questions/14992521/python-weighted-random/14992686#14992686
		self.frequency_stellar_mass = \
			[100.00] + \
			[50.00] + \
			[2.00] * 30 + \
			[1.90] * 30 + \
			[1.80] * 45 + \
			[1.70] * 45 + \
			[1.60] * 45 + \
			[1.50] * 60 + \
			[1.45] * 60 + \
			[1.40] * 60 + \
			[1.35] * 60 + \
			[1.30] * 75 + \
			[1.25] * 75 + \
			[1.20] * 75 + \
			[1.15] * 75 + \
			[1.10] * 75 + \
			[1.05] * 75 + \
			[1.00] * 75 + \
			[0.95] * 75 + \
			[0.90] * 75 + \
			[0.85] * 75 + \
			[0.80] * 75 + \
			[0.75] * 75 + \
			[0.60] * 75 + \
			[0.55] * 45 + \
			[0.50] * 45 + \
			[0.45] * 45 + \
			[0.40] * 45 + \
			[0.35] * 45 + \
			[0.30] * 45 + \
			[0.25] * 225 + \
			[0.20] * 225 + \
			[0.15] * 225 + \
			[0.10] * 1125

		self.classes = {
			'system': self.mod_m.get_class('system'),
			'planet': self.mod_m.get_class('planet'),
			'faction': self.mod_m.get_class('faction'),
			'species': self.mod_m.get_class('species'),
			'faction_personality': self.mod_m.get_class('faction_personality')
		}
		self.classes_freq = {
			'system': self.mod_m.get_freq(self.classes['system']),
			'planet': self.mod_m.get_freq(self.classes['planet'])
		}
		self.fleet_h = fleet_handler.fleet_generator.FleetGenerator(
			mod_m=self.mod_m,
			user_id=self.user_id,
			user_save_id=self.user_save_id
		)

	def clean(self):
		'''
		This is intended to destroy the very, very large lists and
		objects that this thing creates.
		'''
		del self.classes_freq
		del self.frequency_stellar_mass

	def generate(self):
		# Randomly generate universe.
		self.univ = {
			'galaxy_count': config.universe['galaxy_count'],
			'galaxies': [],
			'systems': [],
			'systems_per_galaxy': config.universe['systems_per_galaxy'],
			# Corresponds to a system id.
			'currentSystem': 0,
			# Integers are faster than strings, so we map faction names to this.
			'faction_map': [],
			'fleets': [
				# The first fleet is always the player's fleet.
				{
					# Location in list.
					'id': 0,
					# ID of system currently in.
					'currentSystemID': 0,
					# Military strength, calculated elsewhere.
					'power': 50
				}
			]
		}
		for galaxy in range(self.univ['galaxy_count']):
			self.univ['galaxies'].append(
				self.generate_galaxy(
					galaxy_number=len(self.univ['galaxies'])
				)
			)
		# print('self.univ: ', self.univ)
		self.generate_hyperlane_all()

		# Now, we generate ancient ruins.
		self.generate_ancient_ruins()

		# Now, we generate factions.
		self.generate_factions()

		return self.univ

	def generate_galaxy(self, **kwargs):
		galaxy = {
			'id': kwargs.get('galaxy_number', -1),
			'point': [],
			'globular_clusters': [],
			'open_clusters': []
		}

		# Choose a center for the galaxy.
		# TODO: Make it so that galaxies must be a certain distance away from
		# each other.
		galaxy['point'] = [
			random.randint(0, config.universe['width']),
			random.randint(0, config.universe['height'])
		]

		# TODO:
		# Choose the type of galaxy. There are three types of galaxy:
		# * 0 - Spiral - have a strong, flat shape, with most of the matter
		# 	contained to the galactic disk (or it would, if this was 3D);
		# 	usually has multiple arms in an obvious structure
		# * 1 - Elliptical - mostly an enlongated sphere or circle
		# * 2 - Irregular - any possible shapes
		# * 3 - Ring - ring shape
		galaxy['primary_type'] = random.randint(0, 3)

		# Generate the systems in the galaxy.
		for system in range(self.univ['systems_per_galaxy']):
			self.univ['systems'].append(self.generate_system(
				galaxy=galaxy['id']
			))

		for globular_cluster in range(config.universe['globular_clusters_per_galaxy']):
			globular_cluster_data = {
				'id': globular_cluster,
				'systems': []
			}
			for system in range(config.universe['systems_per_globular_cluster']):
				self.univ['systems'].append(self.generate_system(
					globular_cluster=globular_cluster,
					galaxy=galaxy['id']
				))
			galaxy['globular_clusters'].append(globular_cluster_data)

		for open_cluster in range(config.universe['open_clusters_per_galaxy']):
			open_cluster_data = {
				'id': open_cluster,
				'systems': []
			}
			for system in range(config.universe['systems_per_open_cluster']):
				self.univ['systems'].append(self.generate_system(
					open_cluster=open_cluster,
					galaxy=galaxy['id']
				))
			galaxy['open_clusters'].append(open_cluster_data)

		return galaxy

	def generate_system(self, **kwargs):
		system_data = {
			'id': len(self.univ['systems']),
			'type': random.choice(self.classes_freq['system']),
			# 'mass': random.choice(self.frequency_stellar_mass),
			'satellites': [],
			'hyperlanes': []
		}

		if kwargs.get('galaxy', False):
			system_data['galaxy'] = kwargs.get('galaxy', -1)
		if kwargs.get('open_cluster', False):
			system_data['open_cluster'] = kwargs.get('open_cluster', -1)
		if kwargs.get('globular_cluster', False):
			system_data['globular_cluster'] = kwargs.get('globular_cluster', -1)

		# TODO: Add features for galaxy types (spiral, elliptical...)
		system_data['point'] = [
			random.randint(-config.universe['galaxy_width'], config.universe['galaxy_width']),
			random.randint(-config.universe['galaxy_height'], config.universe['galaxy_height'])
		]

		# Find mass of system.
		mass = random.randint(
			self.classes['system'][system_data['type']]['mass_min'],
			self.classes['system'][system_data['type']]['mass_max']
		)

		# Now, we need to generate satellites, like planets, and their
		# satellites.
		max_satellites = config.universe['max_satellites_per_primary']
		for satellite in range(max_satellites):
			system_data['satellites'].append(self.generate_satellite(
				satellite_num=satellite,
				level=1
			))

		return system_data

	def generate_satellite(self, **kwargs):
		sat_data = {
			'id': kwargs.get('satellite_num', -1),
			# What is the level of the satellite?
			# 0 = the dominant body in a system, so not actually a satellite, so
			# 	don't use this
			# 1 = planet
			# 2 = planet's moon
			# 3 = planet's moon's moon
			'level': kwargs.get('level', 0),
			'type': kwargs.get('sat_type', random.choice(self.classes_freq['planet'])),
			'orbit': {
				'radius': 0,
				'period': 0,
				# I can't spell eccentricity.
				'ecc': 0
			},
			# Gravity. 1 = Earth's gravity.
			'g': 0,
			# The hill sphere; the maximum distance an object can orbit around
			# the satellite. Calculated using:
			# r == a * (m/3M)^(1/float(3))
			# (nth root of x)
			# r = radius
			# a = semi major axis of orbit
			# m = mass of second object
			# M = mass of primary object
			# Also, see http://orbitsimulator.com/formulas/hillsphere.html
			'hill_sphere': 0
		}

		unacceptable_type = True
		tries = 0
		while unacceptable_type:
			tries += 1
			type_try = random.choice(self.classes_freq['planet'])
			if(
				sat_data['level'] >= \
				self.classes['planet'][type_try].get('level_min', 0) and \
				sat_data['level'] <= \
				self.classes['planet'][type_try].get('level_max', 0)
			):
				unacceptable_type = False
				sat_data['type'] = type_try
			else:
				if tries > 100:
					print(
						'Tried too many times, abort is suggested. Sleeping'
						' for .1 seconds so as to not crash. (Try {tryn}) |'
						' Class type is {classname} |'
						' sat_data[level] "{leveln}" does not fit between'
						' "{minn}" and "{maxn}"'
						.format(
							tryn=tries,
							classname=type_try,
							leveln=sat_data['level'],
							minn=self.classes['planet'][type_try].get('level_min', '0" "(DEFAULT)'),
							maxn=self.classes['planet'][type_try].get('level_max', '0" "(DEFAULT)')
						)
					)
					time.sleep(.1)

		planet_type = self.classes['planet'][sat_data['type']]

		initial_mass = random.uniform(
			planet_type.get('initial_mass_min', 0.01),
			planet_type.get('initial_mass_max', 1)
		)
		initial_mass = round(
			initial_mass * planet_type.get('initial_mass_mult', 1),
			2
		)
		sat_data['initial_mass'] = initial_mass

		# The density is used to determine the amount of minerals available on
		# the planet.
		initial_density = random.uniform(
			planet_type.get('initial_density_min', 0.01),
			planet_type.get('initial_density_max', 1)
		)
		initial_density = round(
			initial_density * planet_type.get('initial_density_mult', 1),
			2
		)
		sat_data['initial_density'] = initial_density

		initial_radius = random.uniform(
			planet_type.get('initial_radius_min', 0.01),
			planet_type.get('initial_radius_max', 1)
		)
		initial_radius = round(
			initial_radius * planet_type.get('initial_radius_mult', 1),
			2
		)
		sat_data['initial_radius'] = initial_radius

		initial_grav = 0
		if planet_type.get('gen_gravity', True):
			# initial_grav = sat_data['initial_density'] * sat_data['initial_radius']
			initial_grav = self.get_gravity(
				sat_data['initial_mass'],
				sat_data['initial_radius']
			)
			initial_grav = round(initial_grav, 2)
		else:
			initial_grav = random.uniform(
				planet_type.get('initial_gravity_min', 0.01),
				planet_type.get('initial_gravity_max', 1)
			)
			initial_grav = round(
				initial_grav * planet_type.get('initial_grav_mult', 1),
				2
			)
		sat_data['initial_grav'] = initial_grav

		# Planets usually have three categories of moons:
		# * inner moonlets - small moons close to the planet; within 1 and 2.5
		# 	times the planet's diameter
		# * major moons - between 3 and 15 times the planet's own diameter,
		# 	these are usually fairly large
		# * outer moonlets - anywhere past 20 times the planet's own diameter to
		# 	the hill sphere

		if sat_data['level'] < 3:
			sat_sats = []
			number_of_sats = random.randint(
				planet_type.get('sat_min', 0),
				planet_type.get('sat_max', 2)
			)
			for sat in range(number_of_sats):
				sat_sats.append(self.generate_satellite(
					level=(sat_data['level'] + 1),
					parent=sat_data
				))

		return sat_data

	# Default is what should result in about 1 g.
	# mass is in units of the Earth's mass (e.g., mass=1 == 1 Earth mass)
	# radius is similar (Earth's radius is 6370.99055 kilometers)
	# Returns gravity; unit is Earth's gravity
	def get_gravity(self, mass=1, radius=1):
		return (
			(
				(
					6.67408e-11  # Pull of gravity; gravitational constant.
					*
					(mass * 5.9722e+24)  # Mass of the Earth.
				)
				/
				math.pow(
					(radius * 6368500.0),  # Radius of the Earth.
					2
				)
			)
			/
			9.80665  # See https://web.archive.org/web/20180713200144/https://en.wikipedia.org/wiki/Gravity_of_Earth
		)

	def get_system(self, system):
		return self.univ['systems'][system.get('id', -1)]

	def get_closest_system_to_system(self, **kwargs):
		'''Given coordinates, get the closest system.'''
		x = kwargs.get('x', 0)
		y = kwargs.get('y', 0)
		closest_id = None
		closest = float('inf')

		for system in self.univ['systems']:
			if (system['point'][0] != x) and (system['point'][1] != y):
				system_try = self.get_system(system)
				dist = math.sqrt(
					(system_try.get('point', 0)[0] - x)**2
					+
					(system_try.get('point', 0)[1] - y)**2
				)
				if dist < closest:
					closest = dist
					closest_id = system_try['id']
		return closest_id

	def get_closest_systems(self, **kwargs):
		'''Gets the closest systems to a particular system.'''
		x = kwargs.get('x', 0)
		y = kwargs.get('y', 0)
		num = kwargs.get('num', len(self.univ['systems']))
		close_systems = []
		systems_return = []

		for system in self.univ['systems']:
			system_try = self.get_system(system)
			true_dist = math.sqrt(
				(system_try.get('point', 0)[0] - x)**2
				+
				(system_try.get('point', 0)[1] - y)**2
			)
			if len(close_systems) > num:
				# Then we better start checking to see if this system is closer
				# than any of the others in the list.
				# This tricky loop is for helping remove it from the list. We do
				# not want to create a copy. See http://web.archive.org/web/20180717185056/https://stackoverflow.com/questions/6022764/python-removing-list-element-while-iterating-over-list/6024599
				for system_dist_try_i in range(len(close_systems) - 1, -1, -1):
					system_dist_try = close_systems[system_dist_try_i]
					if true_dist < system_dist_try[0]:
						# This system is closer, so remove that one from the
						# list and put this one in.
						del close_systems[system_dist_try_i]
						close_systems.append([true_dist, system_try['id']])

			else:
				close_systems.append([true_dist, system_try['id']])

		# Return only the systems; not the rarely used distance. That can be
		# extrapolated from the coordinates, which are given anyway.
		systems_return = []
		for close_system in close_systems:
			systems_return.append(self.univ['systems'][close_system[1]])
		return systems_return

	def get_closest_systems_connected(self, **kwargs):
		'''
		Gets the closest systems, but only ones that are already connected by
		a hyperlane. Returns an empty list if used before hyperlanes are
		generated.
		'''
		system_id = kwargs.get('system_id', 0)
		num = kwargs.get('num', len(self.univ['systems']))

		systems_return = []
		for close_system in close_systems:
			systems_return.append(self.univ['systems'][close_system[1]])
		return systems_return

	def generate_hyperlane_all(self):
		'''
		Generates hyperlanes connecting systems to other systems, randomly.
		Mutates the self.univ['systems']. All systems have at least one
		hyperlane.
		'''

		for system in self.univ['systems']:
			# second_system = random.choice(self.univ['systems'])
			second_system = self.univ['systems'][self.get_closest_system_to_system(
				x=system['point'][0],
				y=system['point'][1]
			)]

			self.univ['systems'][system['id']], \
			self.univ['systems'][second_system['id']] = \
				self.generate_hyperlane(
					first_system=system,
					second_system=second_system
				)

		first_system = random.choice(self.univ['systems'])
		closest_wanted_systems = self.get_closest_systems(
			x=first_system['point'][0],
			y=first_system['point'][1],
			num=20
		)
		for system_try in closest_wanted_systems:
			if random.randint(0, 100) > 5:
				(
					self.univ['systems'][first_system['id']], \
					self.univ['systems'][system_try['id']]
				) = \
					self.generate_hyperlane(
						first_system=first_system,
						second_system=system_try
					)

	def generate_hyperlane(self, first_system, second_system):
		'''Generates a hyperlane. Returns the systems without mutating them.'''
		if self.hyperlane_systems_connected(
			first_system=first_system,
			second_system=second_system
		):
			# Already connected, no need to form another hyperlane.
			return first_system, second_system
		else:
			first_system['hyperlanes'].append(second_system['id'])
			second_system['hyperlanes'].append(first_system['id'])
			return first_system, second_system

	def hyperlane_systems_connected(self, first_system, second_system):
		'''
		Checks to see if the first system has a hyperlane pointing to the second
		system, or vice versa.
		'''
		if(
			second_system['id'] in first_system['hyperlanes'] or \
			first_system['id'] in second_system['hyperlanes']
		):
			return True
		return False

	def generate_ancient_ruins(self):
		# TODO: Do
		pass

	def generate_factions(self):
		# Get a list of all the factions, first.
		# print(self.factions)
		faction_number = -1
		for faction_k, faction_v in self.classes['faction'].items():
			faction_number += 1
			# Strings are slow, integers are faster. This maps it.
			self.univ['faction_map'].append(faction_k)
			faction_gen = faction_v['generation']

			# First, get their species.
			primary_species_k = max(
				faction_gen['include_species'],
				key=faction_gen['include_species'].get
			)

			primary_species_v = faction_gen['include_species'][primary_species_k]

			# The actual species data.
			print(self.classes['species'])
			print(primary_species_k)
			primary_species_s = self.classes['species'].get(primary_species_k)

			# First, find a system for them to call "home."
			home_system_id = None
			home_planet_id = None
			unacceptable_planet = True
			unacceptable_planet_tries = 0
			while unacceptable_planet:
				system_try = random.choice(self.univ['systems'])
				sat_try = None
				for sat_try in system_try['satellites']:
					if(
						primary_species_s['planet_pref'] == \
						self.classes['planet'][sat_try['type']]['key']
					):
						home_system_id = system_try['id']
						home_planet_id = sat_try['id']
						unacceptable_planet = False
						break
				unacceptable_planet_tries += 1
				if unacceptable_planet_tries > 100:
					print(
						'Too many of unacceptable_planet (try {tryn}) '
						's {species_pref} != class {planet_class}'
						.format(
							tryn=unacceptable_planet_tries,
							species_pref=primary_species_s['planet_pref'],
							planet_class=self.classes['planet'][sat_try['type']]['key']
						)
					)
					# Don't go so fast that it crashes.
					time.sleep(.1)

			home_system = self.univ['systems'][home_system_id]
			home_system = self.civilize_system(
				system=home_system,
				home=faction_number,
				owner=faction_number,
				original_owner=faction_number
			)


			home_planet = home_system['satellites'][home_planet_id]
			home_planet = self.civilize_satellite(
				satellite=home_planet,
				home=faction_number,
				owner=faction_number,
				original_owner=faction_number
			)

			closest_systems = self.get_closest_systems(
				x=home_system['point'][0],
				y=home_system['point'][1],
				num=random.randint(
					faction_gen['min_systems'],
					faction_gen['max_systems']
				)
			)

			for system_spawn in closest_systems:
				# Find a system close to the home system.
				#
				# fleet_entity_dir = os.path.join(
				# 	os.path.dirname(
				# 		os.path.dirname(os.path.realpath(__file__))
				# 	),
				# 	os.sep+'entities',
				# 	'factions',
				# 	'ganeebian' + os.sep + faction_k
				# )
				# fleet_build_dir = os.path.join(
				# 	os.path.dirname(
				# 		os.path.dirname(os.path.realpath(__file__))
				# 	),
				# 	os.sep+'builds',
				# 	'factions' + os.sep + faction_k
				# )
				# fleet_entity_files = [
				# 	f for f in os.listdir(fleet_entity_dir) \
				# 	if os.path.isfile(os.path.join(fleet_entity_dir, f)) and \
				# 	os.path.splitext(f)[1] == '.json'
				# ]
				# fleet_build_files = [
				# 	f for f in os.listdir(fleet_build_dir) \
				# 	if os.path.isfile(os.path.join(fleet_build_dir, f)) and \
				# 	os.path.splitext(f)[1] == '.json'
				# ]
				# fleet_entities = []
				# fleet_builds = []
				# for fleet_entity_file in fleet_entity_files:
				# 	with open(os.path.join(fleet_entity_dir, fleet_entity_file), 'r') as f:
				# 		fleet_entities.append(json.loads(f.read()))
				# for fleet_build_file in fleet_build_files:
				# 	with open(os.path.join(fleet_build_dir, fleet_build_file), 'r') as f:
				# 		fleet_builds.append(json.loads(f.read()))

				fleet = self.generate_fleet(
					user_id=self.user_id,
					user_save_id=self.user_save_id,
					faction_name='factions'+os.sep+faction_k,
					min_ships=1,
					max_ships=10
				)
				if 'fleets' not in system_spawn:
					system_spawn['fleets'] = []
				system_spawn['fleets'].append(fleet['id'])

	def civilize_system(self, **kwargs):
		'''
		Puts data about a civilization on a system. Does not mutate the system;
		returns it.
		'''
		system = kwargs['system']
		system['owner'] = kwargs.get('owner', -1)

		# Want to know what I dislike about Python? (False == 0) is True. Even
		# in Javascript, (false === 0) is false. So what happens here? You need
		# to do funky things that seem bad. Anyways,
		# Some systems are the home system of certain factions.
		if kwargs.get('home', -1) != -1:
			system['home'] = kwargs['home']

		# Systems could be taken from their original owners. It may be valuable
		# to know who originally owned what.
		if kwargs.get('original_owner', -1) != -1:
			system['original_owner'] = kwargs['original_owner']

		return system

	def civilize_satellite(self, **kwargs):
		'''Puts data about a civilization on the satellite.'''
		satellite = kwargs['satellite']
		# The owner is, by default, the void, space itself, which had an id of
		# -1.
		satellite['owner'] = kwargs.get('owner', -1)
		# Some planets are the home system of certain factions.
		if kwargs.get('home', -1) != -1:
			satellite['home'] = kwargs['home']
		# Planets could get taken over. It may be valuable to note who
		# originally owned what.
		if kwargs.get('original_owner', -1) != -1:
			satellite['original_owner'] = kwargs['original_owner']

		# Buildings are TODO. Really unnecessary at this stage of development;
		# we can just give the factions free money for the time being.
		satellite['buildings'] = ['example_building']
		return satellite

	def add_modifier(self, satellite, **kwargs):
		'''Adds a modifier to the satellite. Does not mutate it; returns it.'''
		# TODO: Do
		return satellite

	def generate_fleet(self, **kwargs):
		return self.fleet_h.generate(**kwargs)


def generate(**kwargs):
	universegen = UniverseGenerator(**kwargs)
	generated = universegen.generate()
	universegen.clean()
	return generated

def main():
	pass

if __name__ == '__main__':
	main()
