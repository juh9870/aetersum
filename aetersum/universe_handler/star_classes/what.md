# What are these files?

As JSON doesn't have comments, it is difficult to describe what these mean.

Here is a simple list:

*   `a.json` - Most common stars that are visible in night sky. White or
 bluish-white.
*   `b.json` - Luminous, blue stars.
*   `carbon.json` - Stars that are near death, these have more carbon than
 oxygen in their atmosphere, resulting in a "sooty," ruby-red appearance.
*   `black_hole.json` - After a very massive star collapses, it can leave behind
 a black hole. Nothing can escape their event horizon, including light.
*   `d.json` - White dwarf; the remnant of a star.
*   `f.json` - Relatively hot, yellowish star.
*   `g.json` - Medium-sized, common, yellow star, like Sol.
*   `k.json` - Orangish stars, cooler than Sol.
*   `m_giant.json` - Some of the rare supergiants or hypergiants are type M,
 despite their size.
*   `m.json` - Very common stars, but so dim that they are rarely visible in the
 night skies of planets. Most are very small.
*   `neutron_star.json` - The remnant of a massive star that collapsed.
*   `o.json` - Very hot, massive stars. Last very shortly.
*   `pulsar.json` - Similar to neutron stars, but even denser. They rotate
 quickly, and spew out powerful radiation. Their rotations are so exact that
 they are sometimes used to measure time.
*   `s.json` - Stars that are between carbon and type M stars; they have about
 an equivalent quantity of carbon and oxygen in their atmosphere.
*   `t.json` - A Brown dwarf; extremely large gas giants, but not large enough
 to become a star.
*   `w.json` - Extremely hot stars with powerful stellar winds. They are no
 longer burning hydrogen. They are bright blue and often surrounded by a nebula.
*   `y.json` - A star just barely large enough to be a star; slightly bigger
 than brown dwarves.
