#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Entity:
	def __init__(self, **kwargs):
		self.entity = {
			'id': kwargs.get('id', -1)
		}
		self.faction_file = kwargs.get('faction_file', {
			# Default object.
			# As it is basically just a JSON file, use JSON formatting.
			"class": "entity",
			"key": "NOTDEFINEDKEY",
			"faction": "NOTDEFINEDFACTION",
			"name": "NOTDEFINEDNAME",
			"icon_scale": 1,
			"sprite_scale": 1,
			"emitters": [

			],
			"layout_width": 9,
			"layout": [
				[0], [4, {"barrel": 0}]
			],
			"barrels": [
				{
					"offsetX": 0,
					"offsetY": 0,
					"rotation": 0,
					"spread": 0
				}
			],
			"shapes": [
				[8, 0, 0, 0, 0, 0]
			],
			"spawn_odd": 10
		})
		

	def get_json(self):
		return self.entity
