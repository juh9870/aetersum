#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from fleet_handler import fleet_generator

def generate_fleet(**kwargs):
	fleet_thing = fleet_generator.generate(**kwargs)
	return fleet_thing

def main():
	pass

if __name__ == '__main__':
	main()
