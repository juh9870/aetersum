#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import entity_handler

import os
import json
import random


class FleetGenerator:
	def __init__(self, mod_m, user_id, user_save_id, **kwargs):
		self.fleets = []
		self.user_id = user_id
		self.user_save_id = user_save_id
		self.mod_m = mod_m
		self.classes = {
			'build': self.mod_m.get_class('build'),
			'entity': self.mod_m.get_class('entity')
		}
		self.classes_freq = {
			'build': self.mod_m.get_freq(self.classes['build']),
			'entity': self.mod_m.get_freq(self.classes['entity'])
		}
		# self.faction_name = kwargs.get('faction_name', -1)
		# self.files_to_load = kwargs.get('files_to_load', [])
		# self.files = []

	def generate(self, **kwargs):
		fleet = {
			"id": -1,
			'entities': []
		}

		# faction_name = self.faction_name
		# directory = None
		# if faction_name == -1:
		# 	directory_given = kwargs.get('directory', -1)
		# 	if directory_given == -1:
		# 		print(
		# 			'No faction or directory given for fleet generator! ',
		# 			faction_name,
		# 			directory_given,
		# 			'Returning None. Prepare for errors.'
		# 		)
		# 		return
		# 	# No need for `else` as it returns.
		# 	directory = directory_given
		# else:
		# 	directory = faction_name

		# Load all the .json files from the directory.
		# self.faction_entities = self.load_dir(dir_from=directory)
		# print(self.faction_entities)

		min_ships = kwargs.get('min_ships', 1)
		max_ships = kwargs.get('max_ships', 1)
		for ship_spawn in range(random.randint(min_ships, max_ships)):
			entity_type = self.classes['entity'][
				random.choice(self.classes_freq['entity'])
			]
			# We also need to get a random build for the ships as well.
			build = None
			build_choice = self.classes['build'][
				random.choice(self.classes_freq['build'])
			]
			# entity_spawn = entity_handler.Entity(
			# 	id=0,
			# 	faction_file=faction_file,
			# 	build=build
			# )
			# fleet['entities'].append(entity_spawn.get_json())
			fleet['entities'].append({
				'type': random.choice(self.classes_freq['entity']),
				'build': random.choice(self.classes_freq['build'])
			})

		# Find out what ID to use for our fleet.
		fleets_dir = \
			os.path.dirname(os.path.dirname(__file__)) \
			+ \
			os.sep + 'storage' + os.sep \
			+ \
			str(self.user_id) \
			+ \
			os.sep + 'saves' + os.sep \
			+ \
			str(self.user_save_id) \
			+ \
			os.sep + 'fleets' + os.sep
		# First, make the fleets dir if needed.
		os.makedirs(fleets_dir, exist_ok=True)
		fleets_files = [
			f for f in os.listdir(fleets_dir) \
			if os.path.isfile(os.path.join(fleets_dir, f)) and \
			os.path.splitext(f)[1] == '.json'
		]
		# Find the latest id.
		latest_file_id = -1
		for file_thing in fleets_files:
			try_id = os.path.splitext(file_thing)[0]
			if int(try_id) > latest_file_id:
				latest_file_id = int(try_id)

		fleet['id'] = latest_file_id + 1
		with open(
			os.path.join(
				fleets_dir,
				str(fleet['id']) + '.json'
			),
			'x'
		) as fleet_file:
			fleet_file.write(json.dumps(fleet))

		return fleet

	def clean(self, **kwargs):
		pass

	def write(self):
		with open('', 'x') as f:
			f.write(self.get_json())

	def get_json(self):
		return {}



def generate(**kwargs):
	fleetgen = FleetGenerator(**kwargs)
	generated = fleetgen.generate()
	fleetgen.clean()
	return generated

def main():
	pass

if __name__ == '__main__':
	main()
