#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse

def main():
	parser = argparse.ArgumentParser(description='Starts the Aetersum server.')
	# Check to see if the files on the server are correct.
	parser.add_argument('validate', action='store_true')
	return parser.parse_args()

if __name__ == '__main__':
	main()
