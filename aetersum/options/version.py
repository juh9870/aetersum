#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-

#######
# Don't edit anything below this line; everything below here is not expected to
# be edited very often. Don't edit anything below this line unless you know what
# you're doing.
#######

python_version = 3.5  # What version of Python is necessary?
phaser_version = '2.11.0'  # What version of Phaser.js is necessary?
phaser_ce = True  # Are we using the community version of Phaser?
lunamalis_version = '0.1'  # What is the version of Lunamalis used?
# This array is the lunamalis_version split into its parts.
lunamalis_version_array = [0, 1]
aetersum_version = '0.1'
aetersum_version_array = [0, 1]
